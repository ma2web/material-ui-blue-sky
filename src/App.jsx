import React, { useState } from "react";
import { BrowserRouter as Route, Switch } from "react-router-dom";
import { ThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import CssBaseline from "@material-ui/core/CssBaseline";
import { I18nextProvider } from "react-i18next";
import i18n from "./i18n";
import themeLight from "./theme/theme-light";
import themeDark from "./theme/theme-dark.json";
import "./assets/fonts/iran-yekan/style.css";
import Login from "./components/Login/login";
import Rtl from "./rtl";
import "./assets/styles/style.scss";
import DashboardLayout from "./components/Dashboard/DashboardLayout";
import PrivateRoute from "./PrivateRoute";
import { Provider } from "react-redux";
import store from "./redux/store";

function App(props) {
  const themeConfig = {
    dir: localStorage.getItem("lang") === "en" ? "ltr" : "rtl",
    fontFamily:
      localStorage.getItem("lang") === "en" ? "sans-serif" : "iranyekan",
    theme:
      localStorage.getItem("themeMode") === "dark" ? themeDark : themeLight,
  };

  const [dir, setDir] = useState(themeConfig.dir);
  const [fontFamily, setFontFamily] = useState(themeConfig.fontFamily);
  const [theme, setTheme] = useState(themeConfig.theme);

  const Apptheme = createMuiTheme(theme, {
    direction: dir,
    typography: {
      fontFamily,
      body1: {
        fontFamily,
      },
      body2: {
        fontFamily,
      },
      button: {
        fontFamily,
      },
      h1: {
        fontFamily,
      },
      h2: {
        fontFamily,
      },
      h3: {
        fontFamily,
      },
      h4: {
        fontFamily,
      },
      h5: {
        fontFamily,
      },
      h6: {
        fontFamily,
      },
    },
  });

  // Set Document Direction and App lang
  if (dir === "rtl") {
    i18n.changeLanguage("fa");
    document.documentElement.setAttribute("dir", "rtl");
  } else {
    i18n.changeLanguage("en");
    document.documentElement.setAttribute("dir", "ltr");
  }

  const toggleThemeLayout = (rtl) => {
    // Set theme Materail UI direction RTL
    // Set FontFamily
    if (rtl) {
      setDir("rtl");
      setFontFamily("iranyekan");
      // Set theme mode to Dark in RTL just for demo
      // setTheme(themeDark);
    } else {
      setDir("ltr");
      setFontFamily("Roboto");
      // Set theme mode to Light in LTR just for demo
      // setTheme(themeLight);
    }
  };

  const toggleMode = (light) => {
    // Set theme mode
    if (light) {
      setTheme(themeLight);
    } else {
      setTheme(themeDark);
    }
  };

  return (
    <Provider store={store}>
      <ThemeProvider theme={Apptheme}>
        <I18nextProvider i18n={i18n}>
          <Rtl>
            <CssBaseline />
            <Switch>
              <Route path='/login' exact>
                <Login
                  themeLayout={toggleThemeLayout}
                  modeLayout={toggleMode}
                />
              </Route>
              <Route path='/' exact>
                <DashboardLayout
                  themeLayout={toggleThemeLayout}
                  modeLayout={toggleMode}
                />
              </Route>
            </Switch>
          </Rtl>
        </I18nextProvider>
      </ThemeProvider>
    </Provider>
  );
}

export default App;
