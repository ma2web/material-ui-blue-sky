export default {
  login: {
    signin: "ورود",
    signup: "اکانت ندارید؟ ثبت نام کنید",
    pass: "گذرواژه",
    ForgotPass: "گذرواژه خود را فراموش کرده اید؟",
    email: "آدرس ایمیل",
    remember: "مرا به خاطر داشته باش",
    lang: "زبان",
    mode: "تم"
  }
};
