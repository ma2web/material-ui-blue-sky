export default {
  login: {
    signin: "login",
    signup: "Don't have an account? Sign Up",
    pass: "Password",
    ForgotPass: "Forgot password?",
    email: "Username",
    remember: "Remember me",
    lang: "Lang",
    mode: "Theme",
  },
};
