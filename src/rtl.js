import React from 'react';
import rtl from 'jss-rtl';
import { create } from 'jss';
import PropTypes from 'prop-types';
import { StylesProvider, jssPreset } from '@material-ui/core/styles';

// Configure JSS to handel RTL
const jss = create({ plugins: [...jssPreset().plugins, rtl()] });

function Rtl(props) {
  const { children } = props;

  return <StylesProvider jss={jss}>{children}</StylesProvider>;
}

Rtl.propTypes = {
  children: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])).isRequired
};

export default Rtl;
