import React from "react";
import Add from "@material-ui/icons/Add";
import { useEffect } from "react";
import PropTypes from "prop-types";
import { useState } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import { Grid } from "@material-ui/core";
import Avatar from "../../../../assets/images/avatar.jpg";

function GridContacts({ get_all_contacts, contacts }) {
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoader(false);
    }, 1000);
  }, []);

  console.log(contacts);

  return (
    <>
      <div className='contact-list add-new'>
        <h1>usrs</h1>
        <div className='float-add-btn'>
          <Add />
        </div>
      </div>

      {loader ? (
        <div className='contact-list'>
          <div className='loader-container'>
            <CircularProgress disableShrink />
          </div>
        </div>
      ) : (
        <Grid container spacing={3}>
          GRID CONTACT
        </Grid>
      )}
    </>
  );
}

export default GridContacts;
