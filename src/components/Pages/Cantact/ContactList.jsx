import React from "react";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import IconButton from "@material-ui/core/IconButton";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import InfoOutlined from "@material-ui/icons/InfoOutlined";
import Add from "@material-ui/icons/Add";
import { useEffect } from "react";
import { useState } from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import DeleteOutlinedIcon from "@material-ui/icons/DeleteOutlined";
import RemoveContact from "./RemoveContact";
import EditModal from "./EditModal";
import ReactPaginate from "react-paginate";
import EditIcon from "@material-ui/icons/Edit";

function ContactList({ showInfoFunc, showAddNewContactFunc }) {
  const contacts = [
    {
      name: "",
      country_code: "",
      number: "",
      _id: "",
      avatar: "",
    },
  ];
  const showAddNewContact = (e) => {
    showAddNewContactFunc();
  };

  const [loader, setLoader] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoader(false);
    }, 1000);
  }, []);

  const [removeContactModal, setShowRemoveContactModal] = useState(false);
  const [contactId, setContactId] = useState("");

  const showRemoveContactModal = (id) => {
    setShowRemoveContactModal(true);
    setContactId(id);
  };

  const closeModal = (e) => {
    setShowRemoveContactModal(false);
  };

  const handlePageClick = (data) => {
    let selected = data.selected;
    console.log(selected);
  };

  const [editModal, setEditModal] = useState(false);

  const [editState, setEditState] = useState({
    id: "",
    name: "",
    cc: "",
    num: "",
    ava: "",
  });

  const showEditModal = (id, name, cc, num, ava) => {
    console.log(id, name, cc, num, ava);

    setEditState({
      id,
      name,
      cc,
      num,
      ava,
    });
    setEditModal(true);
  };

  const showEditContactFunc = (e) => {
    setEditModal(!editModal);
  };

  let users = [
    {
      fullname: "Mohammad Amin Azimi",
      phone: "989102220154",
      email: "mohammadazimi16@gmail.com",
      birthdate: "DEC15 1989",
      age: 30,
      gender: "male",
      avatar: "https://www.ma2web.com/static/media/ma2web.eea5473e.jpeg",
    },
    {
      fullname: "Jane Doe",
      phone: "1234567890",
      email: "jdoe@gmail.com",
      birthdate: "JUN25 1990",
      age: 28,
      gender: "female",
      avatar:
        "https://image.freepik.com/free-vector/young-man-wears-medical-mask-avatar-male-portrait-full-face-illustration-flat-style_276162-45.jpg",
    },
  ];

  return (
    <>
      {editModal ? (
        <EditModal data={editState} closeAddNewContact={showEditContactFunc} />
      ) : null}
      {removeContactModal ? (
        <RemoveContact contactId={contactId} close_modal={closeModal} />
      ) : null}

      <div className="contact-list add-new">
        <h1>users</h1>
        <div className="float-add-btn" onClick={showAddNewContact}>
          <Add />
        </div>
      </div>

      {loader ? (
        <List className="contact-list">
          <div className="loader-container">
            <CircularProgress disableShrink />
          </div>
        </List>
      ) : (
        <>
          <List className="contact-list">
            {users.map((item) => {
              return (
                <ListItem>
                  <ListItemAvatar>
                    <img
                      src={item.avatar}
                      alt="avatar"
                      width={50}
                      height={50}
                      style={{
                        borderRadius: "50%",
                        boxShadow: "0 0 5px 0 rgba(0,0,0,.3)",
                        margin: "3px 12px 0 0",
                      }}
                    />
                  </ListItemAvatar>
                  <ListItemText
                    primary={item.fullname}
                    secondary={item.phone}
                  />
                  <ListItemSecondaryAction className="contacts-actions">
                    <IconButton
                      onClick={() => {
                        console.log(item);
                        showInfoFunc(item);
                      }}
                    >
                      <InfoOutlined />
                    </IconButton>
                    <IconButton>
                      <EditIcon />
                    </IconButton>
                    <IconButton
                      onClick={(e) => {
                        showRemoveContactModal();
                      }}
                    >
                      <DeleteOutlinedIcon />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              );
            })}
          </List>
          <ReactPaginate
            previousLabel={"<"}
            nextLabel={">"}
            breakLabel={"..."}
            breakClassName={"break-me"}
            pageCount={Math.ceil(1 / 10)} // {Math.ceil(pagination.totalPage / pagination.perPage) + 1}
            marginPagesDisplayed={10}
            pageRangeDisplayed={10} // {pagination.perPage}
            onPageChange={handlePageClick}
            containerClassName={"pagination"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </>
      )}
    </>
  );
}

export default ContactList;
