import React from "react";
import ContactList from "./ContactList";
import PropTypes from "prop-types";
import { Grid, Switch, FormControlLabel } from "@material-ui/core";
import ContactInfo from "./ContactInfo";
import { useState } from "react";
import AddNewContact from "./AddNewContact";
import Loader from "../../Elements/Loader/Loader";
import GridContacts from "./NewContact/GridContacts";
import GridOnIcon from "@material-ui/icons/GridOn";
import { useEffect } from "react";

const Contact = ({ get_all_contacts, contacts }) => {
  const [showInfo, setShowInfo] = useState(false);
  const [addNewContact, setAddNewContact] = useState(false);

  const [contactInfo, setContentData] = useState({});

  const showInfoFunc = (contact) => {
    setShowInfo(true);
    setContentData(contact);
  };

  const showAddNewContactFunc = (e) => {
    setAddNewContact(!addNewContact);
  };

  const [loader, setLoader] = useState(false);

  const [state, setState] = useState({
    checkedA: false,
  });

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  useEffect(() => {
    setTimeout(() => {
      setLoader(false);
    }, 1000);
  }, []);

  return (
    <>
      {loader ? (
        <div className="loader-container">
          <div className="loader">
            <Loader />
          </div>
        </div>
      ) : (
        <>
          <div className="centerElem">
            <Switch
              checked={state.checkedA}
              onChange={handleChange}
              name="checkedA"
              inputProps={{ "aria-label": "secondary checkbox" }}
            />{" "}
            <GridOnIcon />
          </div>
          {state.checkedA ? (
            <GridContacts />
          ) : (
            <Grid container spacing={4}>
              <Grid item xs={12} md={6}>
                <ContactList
                  showInfoFunc={showInfoFunc}
                  showAddNewContactFunc={showAddNewContactFunc}
                />
              </Grid>
              {showInfo ? (
                <Grid item xs={12} md={6}>
                  <ContactInfo contactInfo={contactInfo} />
                </Grid>
              ) : null}

              {addNewContact ? (
                <AddNewContact closeAddNewContact={showAddNewContactFunc} />
              ) : null}
            </Grid>
          )}
        </>
      )}
    </>
  );
};

Contact.propTypes = {};

export default Contact;
