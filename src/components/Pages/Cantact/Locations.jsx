import React, { useState, useEffect } from "react";
import PropTypes from "prop-types";
import {
  GoogleMap,
  withScriptjs,
  withGoogleMap,
  Marker,
  InfoWindow
} from "react-google-maps";

const Locations = ({ locations }) => {
  let positions = locations.map(location => {
    return {
      lat: location.lat,
      lng: location.long
    };
  });

  function Map() {
    const [selectedLoc, setSelectedLoc] = useState(null);
    return (
      <>
        {positions.length > 0 ? (
          <GoogleMap
            defaultZoom={10}
            defaultCenter={{
              lat: parseInt(positions[0].lat),
              lng: parseInt(positions[0].lng)
            }}
          >
            {positions.map(position => {
              console.log(position);
              return (
                <Marker
                  position={{
                    lat: parseInt(position.lat),
                    lng: parseInt(position.lng)
                  }}
                  onClick={() => {
                    setSelectedLoc(position);
                  }}
                />
              );
            })}

            {selectedLoc && (
              <InfoWindow
                position={{
                  lat: parseInt(selectedLoc && selectedLoc.lat),
                  lng: parseInt(selectedLoc && selectedLoc.lng)
                }}
              >
                Location detail
              </InfoWindow>
            )}
          </GoogleMap>
        ) : null}
      </>
    );
  }

  const WrappedMap = withScriptjs(withGoogleMap(Map));

  const google_map_api_key = "AIzaSyDmEDgQc58mxZ-peVOxMziGh0gOCoryGTE";
  return (
    <div className='locations'>
      <WrappedMap
        googleMapURL={`https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${google_map_api_key}`}
        loadingElement={<div style={{ height: "100%" }} />}
        containerElement={<div style={{ height: "100%" }} />}
        mapElement={<div style={{ height: "100%" }} />}
      />
    </div>
  );
};

Locations.propTypes = {};

export default Locations;
