import React from "react";
import PropTypes from "prop-types";
import "react-phone-input-2/lib/style.css";
import { useEffect } from "react";

const RemoveContact = ({
  contactId,
  close_modal,
  remove_contact,
  get_all_contacts,
}) => {
  const onRemove = (e) => {
    console.log(contactId);
    // remove_contact(contactId).then(() => {
    //   get_all_contacts(0).then(() => {
    //     close_modal();
    //   });
    // });
  };
  const closeModal = (e) => {
    close_modal();
  };

  useEffect(() => {
    // console.log(contactId);
  }, []);
  return (
    <div className='modal-ma-container'>
      <div className='modal-ma'>
        {/* <div className='modal-ma-image'>
          <img src={AddNewContactImage} alt='add new contact' />
        </div> */}
        <div className='modal-ma-header'>
          <h2>remove contact</h2>
        </div>
        <div className='modal-ma-body'>
          <p>are you sure?</p>
        </div>
        <div className='modal-ma-footer'>
          <button className='btn-ma' onClick={onRemove}>
            REMOVE
          </button>{" "}
          <button className='btn-ma btn-ma-danger' onClick={closeModal}>
            CANCLE
          </button>
        </div>
      </div>
    </div>
  );
};

export default RemoveContact;
