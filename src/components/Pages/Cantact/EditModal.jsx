import React from "react";
import PropTypes from "prop-types";
import { TextField, Button } from "@material-ui/core";
import AddNewContactImage from "../../../assets/images/contact.png";
import { useState } from "react";
import PhoneInput from "react-phone-input-2";
import "react-phone-input-2/lib/style.css";
import Avatar from "../../../assets/images/avatar.png";
import { NotificationManager } from "react-notifications";

const EditModal = ({
  closeAddNewContact,
  update_contact,
  get_all_contacts,
  data,
}) => {
  const closeModal = (e) => {
    closeAddNewContact();
  };

  let _number = `${data.cc}${data.num}`;
  const [countryCode, setCountryCode] = useState(data.cc);
  const [number, setNumber] = useState(_number);
  const [name, setName] = useState(data.name);
  const [avatar, setAvatar] = useState(data.ava);
  const [avatarPreview, setAvatarPreview] = useState(Avatar);

  const handelAvatar = (e) => {
    let file = e.target.files[0];
    console.log(file);
    setAvatar(file);
    setAvatarPreview(URL.createObjectURL(file));
  };

  const handleInputChange = (phoneNumber, selectedCountry) => {
    const formatPhoneNumber = phoneNumber.split(`+${selectedCountry.dialCode}`);
    setNumber(formatPhoneNumber[1]);
    setCountryCode(selectedCountry.dialCode);
  };

  const onSubmit = (e) => {
    e.preventDefault();
    update_contact(data.id, data.name, data.cc, data.num, data.ava).then(
      (res) => {
        if (res.statusCode === 200) {
          get_all_contacts(0).then(() => {
            closeAddNewContact();
            setNumber("");
            setName("");
            setAvatar("");
            setAvatarPreview(Avatar);
            NotificationManager.success("contact updated");
          });
        } else {
          NotificationManager.error(res.message);
        }
      }
    );
  };

  return (
    <div className='modal-ma-container'>
      <div className='modal-ma'>
        <div className='modal-ma-image'>
          <img src={AddNewContactImage} alt='add new contact' />
        </div>
        <div className='modal-ma-header'>
          <h2>edit contact</h2>
        </div>
        <div className='modal-ma-body'>
          <div className='input-container'>
            <TextField
              type='text'
              label='Full Name'
              name='name'
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </div>
          <div className='input-container'>
            <PhoneInput
              country='us'
              value={number}
              inputClass='phoneInput'
              buttonClass='phoneCountry'
              dropdownClass='phoneContainer'
              autoFormat={false}
              style={{ direction: "ltr" }}
              onChange={handleInputChange}
            />
          </div>
          <div
            className='input-container'
            style={{
              display: "flex",
              justifyContent: "flex-start",
              alignItems: "center",
            }}
          >
            <img
              src={avatarPreview}
              alt='Avatar'
              style={{
                width: 60,
                height: 60,
                borderRadius: "50%",
                marginRight: "10px",
              }}
            />
            <input
              accept='image/*'
              id='icon-button-file'
              type='file'
              onChange={(e) => handelAvatar(e)}
              style={{ display: "none" }}
            />
            <label htmlFor='icon-button-file'>
              <Button variant='contained' color='default' component='span'>
                Upload Avatar
              </Button>
            </label>
          </div>
        </div>
        <div className='modal-ma-footer'>
          <button className='btn-ma' onClick={(e) => onSubmit(e)}>
            ACCEPT
          </button>{" "}
          <button className='btn-ma btn-ma-danger' onClick={closeModal}>
            CANCLE
          </button>
        </div>
      </div>
    </div>
  );
};

export default EditModal;
