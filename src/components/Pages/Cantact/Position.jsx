import React from "react";
import PropTypes from "prop-types";
import ListSubheader from "@material-ui/core/ListSubheader";

const Position = props => {
  return (
    <>
      <ListSubheader component='div' id='nested-list-subheader'>
        Target Position
      </ListSubheader>
      <iframe
        src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6474.827325129804!2d51.323608178395155!3d35.76521377629143!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3f8dfd47025da675%3A0xbb0dbf46e3a1e85a!2sNorth%20Punak%2C%20District%205%2C%20Tehran%2C%20Tehran%20Province%2C%20Iran!5e0!3m2!1sen!2s!4v1585496499911!5m2!1sen!2s'
        width='100%'
        height='450'
      ></iframe>
    </>
  );
};

Position.propTypes = {};

export default Position;
