import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import ListSubheader from "@material-ui/core/ListSubheader";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import SimCardIcon from "@material-ui/icons/SimCard";
import ShareIcon from "@material-ui/icons/Share";
import DeveloperModeIcon from "@material-ui/icons/DeveloperMode";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
}));

function PerformAcrions({ contactInfo, simcard_checker, finderInfo }) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(true);

  const handleClick = () => {
    setOpen(!open);
  };

  const simcard_checker_func = (e) => {
    console.log(contactInfo);

    const data = {
      country_code: contactInfo.country_code,
      number: contactInfo.number,
      contact_id: contactInfo._id,
    };

    simcard_checker(data).then(() => {
      console.log("simcard checker called");
    });
  };

  console.log(finderInfo);
  return (
    <List
      component='nav'
      aria-labelledby='nested-list-subheader'
      subheader={
        <ListSubheader component='div' id='nested-list-subheader'>
          Perform Actions
        </ListSubheader>
      }
      className={classes.root}
    >
      <ListItem button onClick={simcard_checker_func}>
        <ListItemIcon>
          <SimCardIcon />
        </ListItemIcon>
        <ListItemText primary='SIMCARD CHECKER' />
      </ListItem>
      <ListItem button>
        <ListItemIcon>
          <DeveloperModeIcon />
        </ListItemIcon>
        <ListItemText primary='GET IMSI' />
      </ListItem>
      <ListItem button onClick={handleClick}>
        <ListItemIcon>
          <ShareIcon />
        </ListItemIcon>
        <ListItemText primary='CELL FINDER' />
      </ListItem>
    </List>
  );
}

export default PerformAcrions;
