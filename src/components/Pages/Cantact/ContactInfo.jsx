import React, { useEffect } from "react";
import PropTypes from "prop-types";
import Avatar from "@material-ui/core/Avatar";
import AccountCircleTwoToneIcon from "@material-ui/icons/AccountCircleTwoTone";
import { Grid, Fab, Button, CircularProgress } from "@material-ui/core";
import Icon from "@material-ui/core/Icon";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import { makeStyles } from "@material-ui/core/styles";
import { green, pink, blue } from "@material-ui/core/colors";
import PerformAcrions from "./PerformActions";
import Position from "./Position";

import NavigationIcon from "@material-ui/icons/Navigation";
import PhoneAndroidIcon from "@material-ui/icons/PhoneAndroid";
import MailOutlineIcon from "@material-ui/icons/MailOutline";
import Locations from "./Locations";
import { useState } from "react";

const ContactInfo = ({
  contactInfo,
  simcard_checker,
  simcardCheckerInfo,
  get_one_contact,
  get_data,
  contact,
  msisdn_imsi,
  imsidata,
}) => {
  const useStyles = makeStyles((theme) => ({
    root: {
      display: "flex",
      "& > *": {
        margin: theme.spacing(1),
      },
    },
    pink: {
      color: theme.palette.getContrastText(pink[500]),
      backgroundColor: pink[500],
    },
    green: {
      color: "#fff",
      backgroundColor: green[500],
    },
    blue: {
      color: "#fff",
      backgroundColor: blue[500],
    },
  }));
  const classes = useStyles();

  const [loadingData, setLoadingData] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoadingData(false);
    }, 1000);
  }, []);

  const [nodata, setNodata] = useState(false);

  console.log(contactInfo);

  return (
    <div className="contact-list">
      {!loadingData && !nodata ? (
        <Grid container className="contact-info">
          <Grid item>
            <img
              src={contactInfo.avatar}
              alt="avatar"
              width={50}
              height={50}
              style={{
                borderRadius: "50%",
                boxShadow: "0 0 5px 0 rgba(0,0,0,.3)",
                margin: "3px 12px 0 0",
              }}
            />
          </Grid>
          <Grid item xs={10}>
            <h2 style={{ marginLeft: 10 }}>{contactInfo.fullname}</h2>
          </Grid>
          <Grid item xs={12}>
            <div className="contact-details">
              <br />
              <div className="details-container">
                <PhoneAndroidIcon />
                <p>{contactInfo.phone}</p>
              </div>
              <div className="details-container">
                <MailOutlineIcon /> {contactInfo.email}
              </div>
              <br />
              <h4 style={{ margin: "5px 0" }}>Information</h4>
              <div>
                <p>
                  <span>Birth Date</span> {contactInfo.birthdate}
                </p>
              </div>
              <div>
                <p>
                  <span>Age</span> {contactInfo.age}
                </p>
              </div>
              <div>
                <p>
                  <span>Gender</span> {contactInfo.gender}
                </p>
              </div>
            </div>
          </Grid>
        </Grid>
      ) : (
        <div className="loader-container">
          <CircularProgress disableShrink />
        </div>
      )}
    </div>
  );
};

export default ContactInfo;
