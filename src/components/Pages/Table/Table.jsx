import React, { useState } from "react";
import PropTypes from "prop-types";
import Loader from "../../Elements/Loader/Loader";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";

const Table = (props) => {
  const [loader, setLoader] = useState(true);

  const items = [1, 2, 3, 4, 5];

  setTimeout(() => {
    setLoader(false);
  }, 2000);
  return (
    <>
      {loader ? (
        <div className='loader-container'>
          <div className='loader'>
            <Loader />
          </div>
        </div>
      ) : (
        <div>
          <div className='container'>
            <h1>Table</h1>
          </div>
          <div style={{ padding: "0 20px" }}>
            <table className='table-ma2web'>
              <thead>
                <tr>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Number</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                {items.map((item) => (
                  <tr>
                    <td>Mohammad Amin Azimi</td>
                    <td>mohammadazimi16@gmail.com</td>
                    <td>+989102220154</td>
                    <td>
                      <EditIcon color='primary' />{" "}
                      <DeleteIcon color='secondary' />
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      )}
    </>
  );
};

Table.propTypes = {};

export default Table;
