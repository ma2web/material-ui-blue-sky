import React, { useState, useEffect } from "react";
import Loader from "../../Elements/Loader/Loader";
import { Grid } from "@material-ui/core";
import CreditCardIcon from "@material-ui/icons/CreditCard";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import AccountTreeIcon from "@material-ui/icons/AccountTree";

const Profile = ({ get_one_user, profile }) => {
  const [loader, setLoader] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setLoader(false);
    }, 1000);
  }, []);

  return (
    <>
      {loader ? (
        <div className='loader-container'>
          <div className='loader'>
            <Loader />
          </div>
        </div>
      ) : (
        <>
          <div className='container'>
            <h1>profile</h1>
          </div>
          <div className='container cell-finder'>
            <Grid container spacing={4}>
              <Grid item xs={12}>
                <div className='info-container'>
                  <AccountCircleIcon /> <span>Username: @username</span>
                </div>
                <div className='info-container'>
                  <AccountTreeIcon /> <span>Role: admin</span>
                </div>
                <div className='credit-container'>
                  <CreditCardIcon /> <span>Credit: 37</span>
                </div>
              </Grid>
            </Grid>
          </div>
        </>
      )}
    </>
  );
};

export default Profile;
