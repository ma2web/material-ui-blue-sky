import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component='div'
      role='tabpanel'
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && <Box p={3}>{children}</Box>}
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    "aria-controls": `nav-tabpanel-${index}`
  };
}

function LinkTab(props) {
  return (
    <Tab
      component='a'
      onClick={event => {
        event.preventDefault();
      }}
      {...props}
    />
  );
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  }
}));

export default function NavTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  let reports = Object.entries(props.reports).map(([key, val]) => {
    let scanner = val.item.scanner.toString();
    let mykeys = key;
    let scaninfo = val.scaninfo;
    return {
      mykeys,
      scanner,
      scaninfo
    };
  });

  return (
    <div className={classes.root}>
      <AppBar position='static'>
        <Tabs
          variant='fullWidth'
          value={value}
          onChange={handleChange}
          aria-label='nav tabs example'
        >
          {reports.map((item, index) => {
            return (
              <LinkTab label={item.mykeys} href='/spam' {...a11yProps(index)} />
            );
          })}
        </Tabs>
      </AppBar>
      {reports.map((item, index) => {
        return (
          <TabPanel value={value} index={index}>
            <p>Scanner: {item.scanner}</p>
            {item.scaninfo.map(info => {
              return <p>Type: {info.item.type}</p>;
            })}
          </TabPanel>
        );
      })}
    </div>
  );
}
