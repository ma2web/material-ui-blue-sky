import React, { useState } from "react";
import PropTypes from "prop-types";
import UC from "../../../assets/images/uc.png";
import Loader from "../../Elements/Loader/Loader";

const UnderConstruction = (props) => {
  const [loader, setLoader] = useState(true);

  setTimeout(() => {
    setLoader(false);
  }, 2000);
  return (
    <>
      {loader ? (
        <div className='loader-container'>
          <div className='loader'>
            <Loader />
          </div>
        </div>
      ) : (
        <div className='uc'>
          <img src={UC} alt='uc' />
        </div>
      )}
    </>
  );
};

UnderConstruction.propTypes = {};

export default UnderConstruction;
