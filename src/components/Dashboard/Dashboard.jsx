import React, { useState } from "react";
import Loader from "../Elements/Loader/Loader";
import { Grid } from "@material-ui/core";
import { useEffect } from "react";
import PieChart1 from "./PieChart1";
import PieChart2 from "./PieChart2";
import PieChart3 from "./PieChart3";
import banner from "../../assets/images/banner.png";
import banner2 from "../../assets/images/banner2.png";

const Dashboard = () => {
  const [loader, setLoader] = useState(true);

  if (localStorage.getItem("token") !== "success") {
    window.location.href = "/login";
  }

  useEffect(() => {
    setLoader(false);
  }, []);
  return (
    <>
      {loader ? (
        <div className="loader-container">
          <div className="loader">
            <Loader />
          </div>
        </div>
      ) : (
        <Grid container className="dashboard">
          <Grid item xs={12} md={4} className="cards">
            <div className="card">
              <div className="card-title">180deg</div>
              <div className="card-body">
                <PieChart1 />
              </div>
            </div>
          </Grid>
          <Grid item xs={12} md={4} className="cards">
            <div className="card">
              <div className="card-title">TwoLevel</div>
              <div className="card-body">
                <PieChart2 />
              </div>
            </div>
          </Grid>
          <Grid item xs={12} md={4} className="cards">
            <div className="card">
              <div className="card-title">RadialBar</div>
              <div className="card-body">
                <PieChart3 />
              </div>
            </div>
          </Grid>

          <Grid item xs={12}>
            <div className="card">
              <img src={banner} alt="banner" />
            </div>
          </Grid>

          <Grid item xs={12} md={3} className="cards">
            <div className="card">
              <div className="card-title">
                <div className="count-title">users</div>
              </div>
              <div className="card-body">
                <div className="count">7,470</div>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} md={3} className="cards">
            <div className="card">
              <div className="card-title">
                <div className="count-title">active</div>
              </div>
              <div className="card-body">
                <div className="count">3,230</div>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} md={3} className="cards">
            <div className="card">
              <div className="card-title">
                <div className="count-title">online</div>
              </div>
              <div className="card-body">
                <div className="count">1,212</div>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} md={3} className="cards">
            <div className="card">
              <div className="card-title">
                <div className="count-title">new user</div>
              </div>
              <div className="card-body">
                <div className="count">45</div>
              </div>
            </div>
          </Grid>

          <Grid item xs={12}>
            <div className="card">
              <img src={banner2} alt="banner2" />
            </div>
          </Grid>
        </Grid>
      )}
    </>
  );
};

export default Dashboard;
