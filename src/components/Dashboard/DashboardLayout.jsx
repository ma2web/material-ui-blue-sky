import React from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Dashboard from "./Dashboard";
import Contact from "../Pages/Cantact/Contact";
import Profile from "../Pages/Profile/Profile";
import Navbar from "../Navbar/Navbar";
import { Grid } from "@material-ui/core";
import TopHeader from "../TopHeader/TopHeader";
import UnderConstruction from "../Pages/Underconstruction/UnderConstruction";
import Elements from "../Pages/Elements/Elements";
import Table from "../Pages/Table/Table";

const DashboardLayout = () => {
  return (
    <Router>
      <Grid container>
        <Grid item xs={12}>
          <TopHeader />
        </Grid>

        <Grid item xs={12} md={2} className='navbar-main-container'>
          <Navbar />
        </Grid>

        <Grid item xs={12} md={10}>
          <div className='content-container'>
            <Switch>
              <Route path='/' component={Dashboard} exact />
              <Route path='/contact' component={Contact} exact />
              <Route path='/profile' component={Profile} exact />
              <Route path='/table' component={Table} exact />
              <Route path='/elements' component={Elements} exact />

              <Route
                path='/coverage-list'
                component={UnderConstruction}
                exact
              />
            </Switch>
          </div>
        </Grid>
      </Grid>
    </Router>
  );
};

export default DashboardLayout;
