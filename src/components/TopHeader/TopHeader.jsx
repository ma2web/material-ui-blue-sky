import React, { useEffect, useState } from "react";
import { Grid } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Search from "@material-ui/icons/Search";
import ProfileMenu from "./ProfileMenu";
import NotificationsIcon from "@material-ui/icons/Notifications";
import CreditCardIcon from "@material-ui/icons/CreditCard";
import CircularProgress from "@material-ui/core/CircularProgress";

const TopHeader = ({ profile, get_one_user }) => {
  // if (!localStorage.getItem("token")) {
  //   window.location.href = "/login";
  // }

  const [spinner, setSpinner] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      setSpinner(false);
    }, 3000);
  }, []);

  return (
    <Grid container className="top-header">
      <Grid item xs={12} md={2}>
        <Grid container spacing={0} alignItems="flex-end" className="search">
          <Grid item xs={1}>
            <Search />
          </Grid>
          <Grid item xs={11}>
            <TextField id="input-with-icon-grid" label="search" />
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} md={10} className="profile-menu">
        <div>
          <ProfileMenu />
        </div>
        <div className="notif">
          <NotificationsIcon />
        </div>
        <div className="credit">
          <div style={{ display: "block", textAlign: "center" }}>credit</div>
          <div className="credit-card">
            <CreditCardIcon />{" "}
            {spinner ? (
              <CircularProgress color="inherit" size={17} />
            ) : (
              <>37$</>
            )}
          </div>
        </div>
      </Grid>
    </Grid>
  );
};

export default TopHeader;
