import React, { useState } from "react";
import PropTypes from "prop-types";
import { useTranslation } from "react-i18next";
import { Grid, TextField } from "@material-ui/core/";
import Logo from "../../assets/images/white-logo.png";
import Xtel from "../../assets/images/xtel.png";
import Wave from "../../assets/images/wave.svg";
import "./Login.css";
import CircularProgress from "@material-ui/core/CircularProgress";
import Switch from "@material-ui/core/Switch";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";

localStorage.setItem("lang", "en");

function Login(props) {
  const { themeLayout, modeLayout } = props;
  // Select lang order base on local storage lang
  const [langOptions] = useState(
    localStorage.getItem("lang")
      ? localStorage.getItem("lang") === "en"
        ? [
            { id: 1, label: "English", value: "en" },
            { id: 2, label: "فارسی", value: "fa" },
          ]
        : [
            { id: 1, label: "فارسی", value: "fa" },
            { id: 2, label: "English", value: "en" },
          ]
      : [
          { id: 2, label: "فارسی", value: "fa" },
          { id: 1, label: "English", value: "en" },
        ],
  );
  const [modeOptions] = useState(
    localStorage.getItem("mode")
      ? localStorage.getItem("mode") === "light"
        ? [
            { id: 1, label: "Light", value: "light" },
            { id: 2, label: "Dark", value: "dark" },
          ]
        : [
            { id: 1, label: "Dark", value: "dark" },
            { id: 2, label: "Light", value: "light" },
          ]
      : [
          { id: 1, label: "Light", value: "light" },
          { id: 2, label: "Dark", value: "dark" },
        ],
  );
  const [lang, setLang] = useState(langOptions[0].value);
  const [mode, setMode] = useState(modeOptions[0].value);
  const { t, i18n } = useTranslation("translation");

  const [loginData, setLoginData] = useState({
    username: "",
    password: "",
    code: "",
  });
  const [success, setSuccess] = useState(false);
  const [redirect, setRedirect] = useState(false);
  const [register, setRegister] = useState(false);
  const [spinner, setSpinner] = useState(false);
  const [checked, setChecked] = React.useState(false);

  const toggleChecked = () => {
    setChecked((prev) => !prev);
    setRegister(!register);
  };

  const handelLocalStorageLang = (appLang) => {
    if (typeof Storage !== "undefined") {
      localStorage.setItem("lang", appLang);
    }
  };
  const handelLocalStorageMode = (appMode) => {
    if (typeof Storage !== "undefined") {
      localStorage.setItem("mode", appMode);
    }
  };

  const handleLangChange = (event) => {
    setLang(event.target.value);
    themeLayout(lang !== "fa");
    if (lang !== "en") {
      i18n.changeLanguage("en");
      handelLocalStorageLang("en");
    } else {
      i18n.changeLanguage("fa");
      handelLocalStorageLang("fa");
    }
  };
  const handleModeChange = (event) => {
    setMode(event.target.value);
    modeLayout(mode !== "light");
    if (mode !== "dark") {
      handelLocalStorageMode("dark");
    } else {
      handelLocalStorageMode("light");
    }
  };
  const onChange = (e) => {
    setLoginData({
      ...loginData,
      [e.target.name]: e.target.value,
    });
  };

  if (redirect) {
    window.location.href = "/";
  }

  return (
    <Grid container component="main" className="login-container">
      <img src={Xtel} alt="xtel" className="xtel" />
      <Grid item xs={12} md={4}>
        <div className="card-container login">
          {register ? (
            <>
              <h1>register</h1>
              <form noValidate>
                <div>
                  <TextField
                    type="text"
                    label="name"
                    name="name"
                    value={loginData.code}
                    onChange={(e) => onChange(e)}
                  />
                </div>
                <div>
                  <TextField
                    type="email"
                    label="email"
                    name="email"
                    value={loginData.code}
                    onChange={(e) => onChange(e)}
                  />
                </div>
                <div>
                  <TextField
                    type="password"
                    label="password"
                    name="password"
                    value={loginData.code}
                    onChange={(e) => onChange(e)}
                  />
                </div>
                <div>
                  <button
                    className="btn-ma btn-danger block"
                    onClick={(e) => {
                      e.preventDefault();
                      setSpinner(true);

                      localStorage.setItem("token", "success");

                      setTimeout(() => {
                        setRedirect(true);
                        setSpinner(false);
                      }, 1500);
                    }}
                  >
                    register{" "}
                    {spinner ? (
                      <CircularProgress color="inherit" disableShrink />
                    ) : null}
                  </button>
                </div>
              </form>
            </>
          ) : (
            <>
              <h1>{t("login.signin")}</h1>
              <form noValidate>
                <div>
                  <TextField
                    type="text"
                    label="Email or username"
                    name="username"
                    value={loginData.username}
                    onChange={(e) => onChange(e)}
                  />
                </div>
                <div>
                  <TextField
                    type="password"
                    label="Password"
                    name="password"
                    value={loginData.password}
                    onChange={(e) => onChange(e)}
                  />
                </div>
                <div>
                  <TextField
                    select
                    label={t("login.lang")}
                    value={lang}
                    onChange={handleLangChange}
                    SelectProps={{
                      native: true,
                    }}
                  >
                    {langOptions.map((item) => {
                      return (
                        <option key={item.id} value={item.value}>
                          {item.label}
                        </option>
                      );
                    })}
                  </TextField>
                </div>
                <div>
                  <button
                    className="btn-ma btn-danger block"
                    onClick={(e) => {
                      e.preventDefault();
                      setSpinner(true);

                      localStorage.setItem("token", "success");

                      setTimeout(() => {
                        setRedirect(true);
                        setSpinner(false);
                      }, 1500);
                    }}
                  >
                    Sign In{" "}
                    {spinner ? (
                      <CircularProgress color="inherit" disableShrink />
                    ) : null}
                  </button>
                </div>
              </form>
            </>
          )}
          <div>
            <FormGroup>
              <FormControlLabel
                control={
                  <Switch
                    size="small"
                    checked={checked}
                    onChange={toggleChecked}
                  />
                }
                label="register"
              />
            </FormGroup>
          </div>
        </div>
      </Grid>
      <Grid item xs={false} md={8} className="text-container-login">
        <Grid item md={6}>
          <div>
            <img src={Logo} alt="wite-logo" />
          </div>
          <div>
            <h2>Welcome to my react admin panel</h2>
            <p>
              this react dashboard template build with React and Material UI
              framework by <a href="http://ma2web.com">Mohammad Amin Azimi</a>
            </p>
          </div>
          <div className="waves">
            <img src={Wave} alt="wave" />
            <img src={Wave} alt="wave" />
            <img src={Wave} alt="wave" />
          </div>
        </Grid>
      </Grid>
    </Grid>
  );
}

export default Login;
