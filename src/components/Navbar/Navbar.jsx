import React from "react";
import PropTypes from "prop-types";
import { Link, NavLink } from "react-router-dom";
import Logo from "../../assets/images/logo.png";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import PeopleIcon from "@material-ui/icons/People";
import appLogo from "../../assets/images/white-logo.png";
import MenuIcon from "@material-ui/icons/Menu";
import HourglassEmptyIcon from "@material-ui/icons/HourglassEmpty";
import SettingsIcon from "@material-ui/icons/Settings";
import TableChartIcon from "@material-ui/icons/TableChart";
import ClearAllIcon from "@material-ui/icons/ClearAll";
import { Dashboard } from "@material-ui/icons";

const Navbar = (props) => {
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(!open);
  };

  const items = [
    {
      primary: "Dashboard",
      to: "/",
      icon: <Dashboard />,
    },
    {
      primary: "Users",
      to: "/contact",
      icon: <PeopleIcon />,
    },
    {
      primary: "Under C",
      to: "/coverage-list",
      icon: <SettingsIcon />,
    },
    {
      primary: "Table",
      to: "/table",
      icon: <TableChartIcon />,
    },
    {
      primary: "Elements",
      to: "/elements",
      icon: <ClearAllIcon />,
    },
  ];

  const [add_class, set_add_class] = React.useState(false);
  return (
    <div className="navbar">
      <List
        component="nav"
        aria-labelledby="nested-list-subheader"
        subheader={
          <div className="subheader">
            <div className="logo">eMa2web</div>
            <div
              className="mobile-menu"
              onClick={() => {
                set_add_class(!add_class);
              }}
            >
              <MenuIcon />
            </div>
          </div>
        }
      >
        {items.map((item) => (
          <NavLink to={item.to} exact className={`${add_class ? " show" : ""}`}>
            <ListItem button>
              <ListItemIcon>{item.icon}</ListItemIcon>
              <ListItemText primary={item.primary} />
            </ListItem>
          </NavLink>
        ))}
      </List>
    </div>
  );
};

Navbar.propTypes = {};

export default Navbar;
