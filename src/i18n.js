import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import en from './assets/locales/en/translation';
import fa from './assets/locales/fa/translation';

const fallbackLng = ['en'];
const availableLanguages = ['fa', 'en'];

i18n
  .use(LanguageDetector) // detect user language

  .use(initReactI18next) // pass the i18n instance to react-i18next.

  .init({
    fallbackLng,
    debug: false,
    lng: 'en',

    whitelist: availableLanguages,
    react: {
      useSuspense: false
    },
    interpolation: {
      escapeValue: false
    },
    resources: {
      en: {
        translation: en
      },
      fa: {
        translation: fa
      }
    }
  });

export default i18n;
